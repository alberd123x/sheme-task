import regular from './Roboto-Regular.ttf';
import bold from './Roboto-Bold.ttf';
import light from './Roboto-Light.ttf';
import thin from './Roboto-Thin.ttf';
import black from './Roboto-Black.ttf';
import medium from './Roboto-Medium.ttf';

const fonts = {
  regular,
  bold,
  light,
  thin,
  black,
  medium,
};

export default fonts;
