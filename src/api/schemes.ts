import axios from 'axios';
import { baseURL } from 'constants/api';
import { IField, IScheme } from 'types/entities';

export const schemesHttp = axios.create({
  baseURL,
});

export const fetchSchemes = (token: string) => schemesHttp.get<IScheme[]>('form', {
  headers: {
    Authorization: `Bearer ${token}`,
  },
});

export const fetchScheme = (id: string | number, token: string) => schemesHttp.get<IScheme>(`form/${id}`, {
  headers: {
    Authorization: `Bearer ${token}`,
  }
});

export const fetchDeleteScheme = (id: string | number, token: string) => schemesHttp.delete<IScheme>(`form/${id}`, {
  headers: {
    Authorization: `Bearer ${token}`,
  }
});

export interface ICreateSchemeBody {
  schema: {
    name: string,
    fields: IField[],
  }
}

export const fetchCreateScheme = (body: ICreateSchemeBody, token: string) => schemesHttp.post('/form', body, {
  headers: {
    Authorization: `Bearer ${token}`,
  }
});

export const fetchUpdateScheme = (body: IScheme, token: string) => schemesHttp.post(`/form/${body.id}`, body, {
  headers: {
    Authorization: `Bearer ${token}`,
  }
});
