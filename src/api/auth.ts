import axios from 'axios';
import { baseURL } from 'constants/api';

export const profilesHttp = axios.create({
  baseURL,
});

export const loginRequest = (username: string, password: string) => {
  return profilesHttp.post<{ access_token: string }>(
    'auth/login',
    {
      username,
      password,
    });
};
