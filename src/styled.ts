import styled, { createGlobalStyle } from 'styled-components';
import { COLORS, FONTS } from 'constants/utils';
import fonts from 'assets/fonts';

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: 'RobotoThin';
    src: ${() => `url(${fonts.thin})`} format('truetype');
    font-weight: 100;
    font-style: normal;
    font-display: swap;
  }

  @font-face {
    font-family: 'RobotoLight';
    src: ${() => `url(${fonts.light})`} format('truetype');
    font-weight: 300;
    font-style: normal;
    font-display: swap;
  }

  @font-face {
    font-family: 'RobotoRegular';
    src: ${() => `url(${fonts.regular})`} format('truetype');
    font-weight: normal;
    font-style: normal;
    font-display: swap;
  }

  @font-face {
    font-family: 'RobotoMedium';
    src: ${() => `url(${fonts.medium})`} format('truetype');
    font-weight: 500;
    font-style: normal;
    font-display: swap;
  }

  @font-face {
    font-family: 'RobotoBold';
    src: ${() => `url(${fonts.bold})`} format('truetype');
    font-weight: bold;
    font-style: normal;
    font-display: swap;
  }

  @font-face {
    font-family: 'RobotoBlack';
    src: ${() => `url(${fonts.black})`} format('truetype');
    font-weight: 900;
    font-style: normal;
    font-display: swap;
  }
  
  div, button, input {
    box-sizing: border-box;
  }

  *,
  *::before,
  *::after {
    box-sizing: border-box;
    font-family: ${FONTS.regular};
  }
  
  #root {
    min-height: 100vh;
  }
  
  ul[class],
  ol[class] {
    padding: 0;
  }
  
  body,
  h1,
  h2,
  h3,
  h4,
  p,
  ul[class],
  ol[class],
  li,
  figure,
  figcaption,
  blockquote,
  dl,
  dd {
    margin: 0;
  }
  
  body {
    min-height: 100vh;
    scroll-behavior: smooth;
    text-rendering: optimizeSpeed;
    line-height: 1.5;
  }
  
  ul[class],
  ol[class] {
    list-style: none;
  }
  
  a:not([class]) {
    text-decoration-skip-ink: auto;
  }
  
  img {
    max-width: 100%;
    display: block;
  }
  
  article > * + * {
    margin-top: 1em;
  }
  
  input,
  button,
  textarea,
  select {
    font: inherit;
  }
  
  @media (prefers-reduced-motion: reduce) {
    * {
      animation-duration: 0.01ms !important;
      animation-iteration-count: 1 !important;
      transition-duration: 0.01ms !important;
      scroll-behavior: auto !important;
    }
  }
`;

export const AppWrapper = styled.div<{ isAuth: boolean }>`
  width: 100%;
  min-height: 100vh;
  background: ${COLORS.background};
  padding-top: ${({ isAuth }) => isAuth ? 80 : 0}px;
`;

export const Container = styled.div`
  max-width: 1080px;
  padding: 0 20px;
  width: 100%;
  margin: 0 auto;
`;
