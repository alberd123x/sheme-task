import React from 'react';
import Login from 'pages/Login';
import Schemes from 'pages/Schemes';
import SchemePage from 'pages/SchemePage';
import SchemeCreate from 'pages/SchemeCreate';

interface IRoute {
  path: RoutesName;
  component: React.ComponentType;
  exact?: boolean;
}

export enum RoutesName {
  LOGIN = '/login',
  SCHEMES = '/schemes',
  SCHEME = '/schemes/:id',
  SCHEME_CREATE = '/scheme-create',
  SCHEME_UPDATE = '/scheme-update/:id',
}

export const publicRoutes: IRoute[] = [
  { path: RoutesName.LOGIN, component: Login, exact: true },
];

export const privateRoutes: IRoute[] = [
  { path: RoutesName.SCHEMES, component: Schemes, exact: true },
  { path: RoutesName.SCHEME, component: SchemePage },
  { path: RoutesName.SCHEME_UPDATE, component: SchemeCreate },
  { path: RoutesName.SCHEME_CREATE, component: SchemeCreate },
];
