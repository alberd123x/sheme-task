export const COLORS = {
  background: '#FAFAFA',
  dark: '#272727',
  blue: '#1390E5',
  darkGrey: '#171719',
  grey: '#F1F1F1',
  lightBlue: '#C5E4F9',
  primaryBlack: '#000000',
  primaryWhite: '#FFFFFF',
  red: '#EB5757',
  svgColor: '#D1D1D1',
  borderGrey: '#C4C4C4',
  green: '#00A241',
};

export const OPACITY = {
  '10': '1A',
  '20': '33',
  '30': '4D',
  '60': '99',
};

export const FONTS = {
  thin: 'RobotoThin, sans-serif',
  light: 'RobotoLight, sans-serif',
  regular: 'RobotoRegular, sans-serif',
  medium: 'RobotoMedium, sans-serif',
  bold: 'RobotoBold, sans-serif',
  black: 'RobotoBlack, sans-serif',
};

export enum Cookies {
  authorization = 'authorization',
}
