export enum FieldType {
  string = 'string',
  password = 'password',
  select = 'select',
  number = 'number',
  tel = 'telephone',
  checkbox = 'checkbox',
}

export interface IOption {
  key: string;
  value: string;
}

export interface IField {
  key: string;
  label: string;
  type: FieldType;
  validation: {
    required?: boolean;
    min?: number;
    max?: number;
    pattern?: string;
  };
  options?: IOption[];
}

export interface IScheme {
  id: number;
  schema: {
    name: string,
    fields: IField[],
  }
}
