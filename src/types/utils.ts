import { FlattenSimpleInterpolation } from 'styled-components';

export type TCustomStyles = string | FlattenSimpleInterpolation;
