import React, { FC, useEffect } from 'react';
import AppRouter from 'router';
import { useTypedSelector } from 'hooks/useTypedSelector';
import Navbar from 'components/Navbar';
import { PersistGate } from 'redux-persist/integration/react';
import { persistor } from 'store';
import { setCookie } from 'helpers/cookie';
import { Cookies } from 'constants/utils';
import * as ST from './styled';

const App: FC = () => {
  const { isAuth, token } = useTypedSelector(state => state.auth);

  useEffect(() => {
    if (token) {
      setCookie({
        name: Cookies.authorization,
        value: token,
      });
    }
  }, [token]);

  return (
    <PersistGate persistor={persistor}>
      <ST.AppWrapper isAuth={isAuth}>
        {isAuth && <Navbar />}
        <AppRouter />
      </ST.AppWrapper>
    </PersistGate>
  );
};

export default App;
