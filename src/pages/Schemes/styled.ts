import styled from 'styled-components';
import { Container } from 'styled';
import { COLORS, FONTS } from 'constants/utils';
import Button from 'components/Button';

export const Wrapper = styled(Container)`
  display: flex;
  flex-direction: column;
`;

export const Title = styled.h2`
  font-size: 28px;
  line-height: 40px;
  font-family: ${FONTS.regular};
  color: ${COLORS.primaryBlack};
  margin: 40px 0;
`;

export const StyledButton = styled(Button)`
  max-width: fit-content;
`;

export const Schemes = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  margin-bottom: 40px;
`;
