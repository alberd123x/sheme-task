import styled from 'styled-components';
import { COLORS } from 'constants/utils';

const HEIGHT = 47;

export const Container = styled.div`
  position: relative;
  display: flex;
`;

export const Name = styled.div<{ isOpen?: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
  padding: 13px 20px 14px 20px;
  background: ${COLORS.primaryWhite};
  height: 100%;
  font-size: 16px;
  line-height: 20px;
  z-index: 1;
  margin-bottom: ${({ isOpen }) => isOpen ? HEIGHT : 0}px;
  border-bottom: 1px solid ${COLORS.grey};
  transition: margin-bottom .3s ease;
  cursor: pointer;
`;

export const EditBlock = styled.div<{ isOpen?: boolean }>`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  column-gap: 30px;
  position: absolute;
  top: ${({ isOpen }) => isOpen ? HEIGHT : 0}px;
  left: 0;
  z-index: 0;
  background: ${COLORS.grey};
  background-blend-mode: multiply;
  width: 100%;
  padding: 12px 20px 15px 20px;
  max-height: ${HEIGHT}px;
  height: 100%;
  border-bottom: 1px solid ${COLORS.grey};
  transition: top .3s ease;
`;

export const Button = styled.div<{ isDelete?: boolean }>`
  font-size: 14px;
  line-height: 20px;
  cursor: pointer;
  color: ${({ isDelete }) => isDelete ? COLORS.red : COLORS.blue};
  transition: opacity .3s ease;
  
  :hover {
    opacity: 0.6;
  }
`;
