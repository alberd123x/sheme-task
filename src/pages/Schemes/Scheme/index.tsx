import React, { FC, useState } from 'react';
import { IScheme } from 'types/entities';
import { useHistory } from 'react-router-dom';
import { useTypedSelector } from 'hooks/useTypedSelector';
import { useActions } from 'hooks/useActions';
import * as ST from './styled';

interface IProps {
  item: IScheme;
}

const Scheme: FC<IProps> = ({ item: { id, schema } }) => {
  const { deleteScheme } = useActions();
  const token = useTypedSelector(state => state.auth.token);
  const [isOpen, setIsOpen] = useState(false);
  const { push } = useHistory();

  const toggleOpen = () => {
    setIsOpen(prev => !prev);
  };

  const view = () => {
    push(`/schemes/${id}`);
  };

  const deleteSchema = () => {
    if (id && token) {
      deleteScheme({ id, token });
    }
  };

  const edit = () => {
    push(`/scheme-update/${id}`);
  };

  return (
    <ST.Container>
      <ST.Name isOpen={isOpen} onClick={toggleOpen}>{schema.name}</ST.Name>
      <ST.EditBlock isOpen={isOpen}>
        <ST.Button onClick={view}>Посмотреть</ST.Button>
        <ST.Button onClick={edit}>Изменить</ST.Button>
        <ST.Button isDelete onClick={deleteSchema}>Удалить</ST.Button>
      </ST.EditBlock>
    </ST.Container>
  );
};

export default Scheme;
