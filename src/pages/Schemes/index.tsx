import React, { FC, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { RoutesName } from 'router/routes';
import { useActions } from 'hooks/useActions';
import { useTypedSelector } from 'hooks/useTypedSelector';
import Scheme from 'pages/Schemes/Scheme';
import * as ST from './styled';

const Schemes: FC = () => {
  const { push } = useHistory();
  const { getSchemes } = useActions();
  const { list } = useTypedSelector(state => state.schemes);
  const token = useTypedSelector(state => state.auth.token);

  useEffect(() => {
    if (token) {
      getSchemes({ token });
    }
    // eslint-disable-next-line
  }, [token]);

  const handleClickButton = () => {
    push(RoutesName.SCHEME_CREATE);
  };

  return (
    <ST.Wrapper>
      <ST.Title>Мои схемы</ST.Title>
      <ST.Schemes>
        {list.map(item => <Scheme key={item.id} item={item} />)}
      </ST.Schemes>
      <ST.StyledButton text='Создать схему' onClick={handleClickButton} />
    </ST.Wrapper>
  );
};

export default Schemes;
