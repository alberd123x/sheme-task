import styled from 'styled-components';
import { COLORS, OPACITY } from 'constants/utils';
import Button from 'components/Button';

export const Container = styled.main`
  width: 100%;
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const LoginBlock = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 510px;
  width: 100%;
  background: ${COLORS.primaryWhite};
  border: 1px solid ${COLORS.dark + OPACITY['10']};
  border-radius: 5px;
  max-height: 520px;
  padding: 40px 88px 60px;
`;

export const Title = styled.h3`
  font-size: 18px;
  line-height: 28px;
  color: ${COLORS.primaryBlack};
  margin: 32px 0 0;
`;

export const InputsBlock = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  row-gap: 20px;
  margin-top: 20px;
  margin-bottom: 40px;
`;

export const StyledButton = styled(Button)`
  max-width: 172px;
`;
