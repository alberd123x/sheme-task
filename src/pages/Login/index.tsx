import React, { FC, useEffect, useState } from 'react';
import Input, { InputScheme } from 'components/Input';
import Logo from 'components/Logo';
import * as ST from './styled';
import { useActions } from 'hooks/useActions';
import { FieldType } from 'types/entities';

const Login: FC = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [disabledButton, setDisabledButton] = useState(true);
  const { login } = useActions();

  useEffect(() => {
    setDisabledButton(!username || !password);
  }, [username, password]);

  const handlePasswordChange = (value: string) => {
    setPassword(value);
  };

  const handleUsernameChange = (value: string) => {
    setUsername(value);
  };

  const submit = async () => {
    await login({
      password,
      username,
    });
  };

  return (
    <ST.Container>
      <ST.LoginBlock>
        <Logo />
        <ST.Title>Вход</ST.Title>
        <ST.InputsBlock>
          <Input scheme={InputScheme.login} label='Логин' value={username} onChange={handleUsernameChange} />
          <Input scheme={InputScheme.login} type={FieldType.password} label='Пароль' value={password} onChange={handlePasswordChange} />
        </ST.InputsBlock>
        <ST.StyledButton text='Войти' disabled={disabledButton} onClick={submit} />
      </ST.LoginBlock>
    </ST.Container>
  );
};

export default Login;
