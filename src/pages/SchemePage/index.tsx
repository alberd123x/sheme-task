import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import { useTypedSelector } from 'hooks/useTypedSelector';
import { useActions } from 'hooks/useActions';
import Input from 'components/Input';
import { FieldType, IField } from 'types/entities';
import Button from 'components/Button';
import * as ST from './styled';
import Checkbox from 'components/Checkbox';
import Select from 'components/Select';

interface TInputs {
  [key: string]: string;
}

interface TCheckboxes {
  [key: string]: boolean;
}

const SchemePage = () => {
  const { id } = useParams<{ id: string | undefined }>();
  const token = useTypedSelector(state => state.auth.token);
  const currentScheme = useTypedSelector(state => state.schemes.currentScheme);
  const [fieldErrors, setFieldErrors] = useState<TCheckboxes>({});
  const [inputs, setInputs] = useState<TInputs>({});
  const [selects, setSelects] = useState<TInputs>({});
  const [checkboxes, setCheckboxes] = useState<TCheckboxes>({});
  const [notValid, setNotValid] = useState<boolean | undefined>();
  const { getScheme } = useActions();

  useEffect(() => {
    if (token && id) {
      fetchScheme(id, token);
    }
    // eslint-disable-next-line
  }, [id, token]);

  useEffect(() => {
    getValues();
    // eslint-disable-next-line
  }, [currentScheme]);

  const fetchScheme = async (id: string, token: string) => {
    getScheme({ id, token, cb: clearValues });
  };

  const clearValues = () => {
    setInputs({});
    setCheckboxes({});
  };

  const check = () => {
    const notValid = currentScheme?.schema.fields.some(field => !checkField(field));
    setNotValid(notValid);
  };

  const checkField = (field: IField): boolean => {
    let isValid = true;
    let value: string | boolean = '';
    const required = field.validation.required;
    const min = field.validation.min;
    const max = field.validation.max;
    const pattern = field.validation.pattern;
    switch (field.type) {
      case FieldType.checkbox:
        value = checkboxes[field.key];
        break;
      case FieldType.select:
        value = selects[field.key];
        break;
      default:
        value = inputs[field.key];
        break;
    }
    if (required) {
      isValid = typeof value === 'boolean' ? value : !!value.length;
    }
    if (typeof value !== 'boolean') {
      if (min) {
        isValid = field.type === FieldType.number ? +value > min : value.length > min;
      }

      if(!isValid) {
        setFieldErrors(prev => ({ ...prev, [field.key]: true }));
        return false;
      }

      if (max) {
        isValid = field.type === FieldType.number ? +value < max : value.length < max;
      }

      if (pattern) {
        isValid = new RegExp(pattern).test(value);
      }
    }
    setFieldErrors(prev => ({ ...prev, [field.key]: !isValid }));
    return isValid;
  };

  const getValues = () => {
    if (currentScheme) {
      const { schema: { fields } } = currentScheme;
      return fields
        .forEach(field => {
          switch (field.type) {
            case FieldType.checkbox:
              setCheckboxes(prev => ({ ...prev, [field.key]: false }));
              break;
            case FieldType.select:
              setSelects(prev => ({ ...prev, [field.key]: '' }));
              break;
            default:
              setInputs(prev => ({ ...prev, [field.key]: '' }));
              break;
          }
        });
    }
  };

  const renderItems = (values: { [key: string]: any }, type?: FieldType) => {
    const fields = currentScheme?.schema.fields;
    if (fields) {
      const items: JSX.Element[] = [];
      for (let key in values) {
        const field = fields.find(item => item.key === key);
        if (field) {
          switch (type) {
            case FieldType.select:
              const options = field.options || [];
              items.push((
                <Select
                  options={options}
                  label={field.label}
                  value={options.find(o => o.value === selects[field.key])?.value}
                  onChange={v => onSelectChange(field.key, v)}
                  placeholder={field.label}
                />
              ));
              break;
            case FieldType.checkbox:
              items.push((
                <Checkbox
                  id={field.key}
                  checked={values[key]}
                  onChange={() => onCheckboxChange(key)}
                  label={field.label}
                />
              ));
              break;
            default:
              items.push((
                <Input
                  required={field.validation.required}
                  min={field.validation.min}
                  max={field.validation.max}
                  isError={fieldErrors[field.key]}
                  type={field.type}
                  value={values[key]}
                  onChange={v => onChange(key, v)}
                  label={field.label}
                />
              ));
              break;
          }

        }
      }
      return items;
    }
  };

  const onChange = (key: string, value: string) => {
    setInputs(prev => ({ ...prev, [key]: value}));
  };

  const onCheckboxChange = (key: string) => {
    setCheckboxes(prev => ({ ...prev, [key]: !prev[key] }));
  };

  const onSelectChange = (key: string, value: string) => {
    setSelects(prev => ({ ...prev, [key]: value}));
  };

  return (
    <ST.StyledContainer>
      <ST.Block>
        <ST.BlockContainer>
          <ST.Name>{currentScheme?.schema.name}</ST.Name>
          {renderItems(inputs)}
          {renderItems(checkboxes, FieldType.checkbox)}
          {renderItems(selects, FieldType.select)}
          <Button text='Валидация' onClick={check} />
          <ST.Plank notValid={notValid}>
            {notValid
              ? 'Валидация не пройдена.'
              : 'Валидация пройдена успешно!'
            }
          </ST.Plank>
        </ST.BlockContainer>
      </ST.Block>
    </ST.StyledContainer>
  );
};

export default SchemePage;
