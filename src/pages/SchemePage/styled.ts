import styled from 'styled-components';
import { Container } from 'styled';
import { COLORS } from 'constants/utils';
import successIcon from './success.svg';
import errorIcon from './error.svg';

export const StyledContainer = styled(Container)`
  padding-top: 36px;
`;

export const Block = styled.div`
  width: 100%;
  padding: 36px;
  background: ${COLORS.primaryWhite};
  border-radius: 5px;
`;

export const Name = styled.div`
  font-size: 28px;
  line-height: 40px;
  color: ${COLORS.primaryBlack};
`;

export const BlockContainer = styled.div`
  display: flex;
  flex-direction: column;
  row-gap: 20px;
  max-width: 562px;
  width: 100%;
`;

export const Plank = styled.div<{ notValid?: boolean }>`
  display: ${({ notValid }) => typeof notValid === 'undefined' ? 'none' : 'flex'};
  align-items: center;
  column-gap: 8px;
  padding: 16px 17px 16px 8px;
  width: 100%;
  font-size: 16px;
  line-height: 24px;
  margin-top: 20px;
  background: ${COLORS.primaryWhite};
  border: 1px solid ${({ notValid }) => notValid ? COLORS.red : COLORS.green};
  color: ${({ notValid }) => notValid ? COLORS.red : COLORS.green};
  border-radius: 5px;
  
  :before {
    content: '';
    min-width: 24px;
    min-height: 24px;
    background: url(${({ notValid }) => notValid ? errorIcon : successIcon}) no-repeat center center;
  }
`;


