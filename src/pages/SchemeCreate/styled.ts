import styled, { css } from 'styled-components';
import { Container } from 'styled';
import { COLORS, FONTS } from 'constants/utils';

export const StyledContainer = styled(Container)`
  padding: 40px 20px;
`;

export const Title = styled.h2<{ mb?: number }>`
  font-size: 28px;
  line-height: 40px;
  color: ${COLORS.primaryBlack};
  margin: 0 0 20px;
  ${({ mb }) => mb && css`margin-bottom: ${mb}px;`}
  font-family: ${FONTS.light};
`;

export const Text = styled.div`
  font-size: 16px;
  line-height: 24px;
  display: flex;
  align-items: center;
  color: ${COLORS.primaryBlack};
  opacity: 0.5;
  margin-bottom: 20px;
`;

export const Footer = styled.div`
  display: flex;
  width: 100%;
  justify-content: space-between;
`;

export const Border = styled.div`
  width: 100%;
  height: 1px;
  background: ${COLORS.borderGrey};
  margin-top: 30px;
  margin-bottom: 40px;
`;
