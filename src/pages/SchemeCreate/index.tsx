import React, { useEffect, useState } from 'react';
import { css } from 'styled-components';
import { v4 as uuidv4 } from 'uuid';
import Input from 'components/Input';
import Button, { ButtonSchemes } from 'components/Button';
import { FieldType, IField, IOption, IScheme } from 'types/entities';
import FieldAccordion from 'pages/SchemeCreate/FieldAccordion';
import { useActions } from 'hooks/useActions';
import { ICreateSchemeBody } from 'api/schemes';
import { useTypedSelector } from 'hooks/useTypedSelector';
import { useLocation, useParams } from 'react-router-dom';
import * as ST from './styled';

export interface CreatField {
  tempId: string;
  key: string;
  label: string;
  type: string;
  validation: {
    required?: boolean;
    min?: number;
    max?: number;
    pattern?: string;
  };
  options?: IOption[];
}

const initialField: CreatField = {
  tempId: uuidv4(),
  options: [],
  key: '',
  type: '',
  label: '',
  validation: {},
};

const prepareCreateField = ({ key, label, validation, options }: CreatField, type: FieldType): IField => ({
  key,
  label,
  type,
  validation,
  options,
});

const prepareField = (field: IField): CreatField => ({
  tempId: uuidv4(),
  ...field,
  validation: field.validation && { ...field.validation },
  options: field.options && [ ...field.options ],
});

export type TFieldKey = keyof typeof initialField;

export type TValidationKey = 'required' | 'min' | 'max' | 'pattern';

const SchemeCreate = () => {
  const [name, setName] = useState('');
  const [fields, setFields] = useState<CreatField[]>([]);
  const [disableCreate, setDisableCreate] = useState(true);
  const { token } = useTypedSelector(state => state.auth);
  const { currentScheme } = useTypedSelector(state => state.schemes);
  const { createScheme, updateScheme, getScheme } = useActions();
  const { id } = useParams<{ id: string | undefined }>();
  const { pathname } = useLocation();
  const isUpdate = pathname === `/scheme-update/${id}`;

  useEffect(() => {
    if (isUpdate && currentScheme) {
      setFields(currentScheme.schema.fields.map(prepareField));
      setName(currentScheme.schema.name);
    }
    // eslint-disable-next-line
  }, [currentScheme]);

  useEffect(() => {
    if (id && token && !currentScheme) {
      getScheme({ id, token });
    }
    const notValidFields = fields.some(item => !item.key || !item.label || !item.type);
    const notValidOptions = fields.some(item => !item.options ? false : item.options.some(option => !option.key || !option.value));
    setDisableCreate(!name || notValidFields || notValidOptions);
    // eslint-disable-next-line
  }, [fields, name, currentScheme]);

  const submit = async () => {
    if (token) {
      if (isUpdate && id) {
        const body: IScheme = {
          id: +id,
          schema: {
            name,
            fields: fields.map(field => prepareCreateField(field, field.type as FieldType)),
          },
        };
        await updateScheme({ body, token });
      } else {
        const body: ICreateSchemeBody = {
          schema: {
            name,
            fields: fields.map(field => prepareCreateField(field, field.type as FieldType)),
          }
        };
        await createScheme({ body, token });
      }
    }
  };

  const setKey = (i: number, key: TFieldKey, value: any) => {
    const newFields = [...fields];
    newFields[i][key] = value;
    setFields(newFields);
  };

  const setValidation = (i: number, key: TValidationKey, value?: any) => {
    const newFields = [...fields];
    newFields[i].validation[key] = typeof value === 'undefined' ? !newFields[i].validation[key] : value;
    setFields(newFields);
  };

  const addOption = (i: number) => {
    const newFields = [...fields];
    newFields[i].options = newFields[i].options || [];
    // @ts-ignore
    newFields[i].options.push({ key: '', value: '' });
    setFields(newFields);
  };

  const changeOption = (i: number, indexOption: number, key: 'key' | 'value', value: string) => {
    const newFields = [...fields];
    if (newFields[i]?.options) {
      // @ts-ignore
      newFields[i].options[indexOption][key] = value;
    }
    setFields(newFields);
  };

  const deleteOption = (i: number, indexOption: number) => {
    const newFields = [...fields];
    if (newFields[i]?.options) {
      // @ts-ignore
      newFields[i].options.splice(indexOption, 1);
    }
    setFields(newFields);
  };

  const deleteField = (id: string) => {
    setFields(prevState => [...prevState.filter(field => field.tempId !== id)]);
  };

  const addNewField = () => {
    setFields(prev => [...prev, {
      tempId: uuidv4(),
      options: [],
      key: '',
      type: '',
      label: '',
      validation: {},
    }]);
  };

  return (
    <ST.StyledContainer>
      <ST.Title>{isUpdate ? 'Изменить схему' : 'Новая схема'}</ST.Title>
      <Input
        value={name}
        onChange={setName}
        required
        label={'Название схемы'}
        maxWidth='442px'
        placeholder={'Укажите название схемы'}
      />
      <ST.Border />
      <ST.Title mb={8}>Свойства схемы</ST.Title>
      <ST.Text>Схема должна содержать хотя бы одно свойство</ST.Text>
      {fields.map((field, i) => (
        <FieldAccordion
          key={field.tempId}
          title={`Свойство ${i + 1}: ${field.label}`}
          onChangeName={v => setKey(i, 'label', v)}
          onChangeKey={v => setKey(i, 'key', v)}
          onChangeType={v => setKey(i, 'type', v)}
          toggleRequired={() => setValidation(i, 'required')}
          onChangeMin={v => setValidation(i, 'min', v ? +v : null)}
          onChangeMax={v => setValidation(i, 'max', v ? +v : null)}
          onChangeRegexp={v => setValidation(i, 'pattern', v)}
          onChangeOptionKey={(indexOption, v) => changeOption(i, indexOption, 'key', v)}
          onChangeOptionValue={(indexOption, v) => changeOption(i, indexOption, 'value', v)}
          deleteOption={indexOption => deleteOption(i, indexOption)}
          addOption={() => addOption(i)}
          onDelete={() => deleteField(field.tempId)}
          field={field}
        />
      ))}
      <ST.Border />
      <ST.Footer>
        <Button
          onClick={addNewField}
          scheme={ButtonSchemes.inline}
          text='Добавить новое свойство'
          customStyles={css`max-width: 288px;`}
        />
        <Button onClick={submit} disabled={disableCreate} text={isUpdate ? 'Изменить схему' : 'Сохранить схему'} customStyles={css`max-width: 210px;`} />
      </ST.Footer>
    </ST.StyledContainer>
  );
};

export default SchemeCreate;
