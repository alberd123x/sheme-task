import React, { FC, useEffect, useState } from 'react';
import { FieldType } from 'types/entities';
import Accordion from 'components/Accordion';
import Input from 'components/Input';
import Select from 'components/Select';
import { IValidationValue, OPTIONS, VALIDATIONS } from 'pages/SchemeCreate/FieldAccordion/data';
import Switch from 'components/Switch';
import { CreatField } from 'pages/SchemeCreate/index';
import Button, { ButtonSchemes } from 'components/Button';
import crossIcon from 'assets/images/cross-lines.svg';
import * as ST from './styled';

type TChangeFunc = (v: string) => void;
type TChangeFuncIndex = (i: number, v: string) => void;
type TChangeFuncNull = (v: string | null) => void;
type TVoidFunc = () => void;

interface IProps {
  title: string;
  onChangeName: TChangeFunc;
  onChangeKey: TChangeFunc;
  onChangeType: TChangeFunc;
  onChangeMin: TChangeFuncNull;
  onChangeMax: TChangeFuncNull;
  onChangeRegexp: TChangeFuncNull;
  onChangeOptionKey: TChangeFuncIndex;
  onChangeOptionValue: TChangeFuncIndex;
  deleteOption: (i: number) => void;
  addOption: TVoidFunc;
  toggleRequired: TVoidFunc;
  onDelete: TVoidFunc;
  field: CreatField;
}

const FieldAccordion: FC<IProps> = ({
  field,
  title,
  onDelete,
  onChangeName,
  onChangeKey,
  onChangeType,
  toggleRequired,
  onChangeMin,
  onChangeMax,
  onChangeRegexp,
  addOption,
  onChangeOptionKey,
  onChangeOptionValue,
  deleteOption,
}) => {
  const [open, setOpen] = useState(false);
  const [validation, setValidation] = useState<IValidationValue | undefined>();
  const optionsKeys: string[] = field.options ? field.options.map(o => o.key) : [];
  const isDisableOptionButton = !!field && (!field.options || !field.options.length ? false : !field.options[field.options.length - 1]?.key);

  useEffect(() => {
    setValidation(VALIDATIONS[field.type]);
  }, [field.type]);

  useEffect(() => {
    if (validation?.regexp) {
      onChangeRegexp(null);
    }
    if (validation?.minMax) {
      onChangeMax(null);
      onChangeMin(null);
    }

    if (field?.type !== FieldType.select) {
      field.options = undefined;
    }
    // eslint-disable-next-line
  }, [validation]);

  const toggle = () => {
    setOpen(prev => !prev);
  };

  return (
    <Accordion open={open} title={title} toggle={toggle} onDelete={onDelete}>
      <ST.Body>
        <ST.DefaultSettingsBlock>
          <Input required value={field.key} onChange={onChangeKey} label={'Ключ свойства'} placeholder='Укажите ключ свойства' />
          <Input required value={field.label} onChange={onChangeName} label={'Название свойства'} placeholder='Укажите название свойства' />
          <Select
            required
            label='Поле для отображения'
            options={OPTIONS}
            placeholder='Выберите поле для отображения'
            onChange={onChangeType}
            value={OPTIONS.find(opt => opt.value === field.type)?.key}
          />
          {field.type === FieldType.select && (
            <ST.FieldOptions>
              {field.options && field.options.map((option, i) => (
                <ST.FieldOption>
                  <Input
                    value={option.value}
                    onChange={v => onChangeOptionValue(i, v)}
                    icon={crossIcon}
                    onIconClick={() => deleteOption(i)}
                  />
                  <Input
                    isError={optionsKeys.lastIndexOf(option.key) !== optionsKeys.indexOf(option.key)}
                    errorMessage={'Такой ключ уже существует'}
                    value={option.key}
                    onChange={v => onChangeOptionKey(i, v)}
                  />
                </ST.FieldOption>
              ))}
              <Button disabled={isDisableOptionButton} onClick={addOption} scheme={ButtonSchemes.inline} text='Добавить вариант' />
            </ST.FieldOptions>
          )}
        </ST.DefaultSettingsBlock>
        {validation && (
          <ST.Validations>
            {validation.required && (
              <ST.ValidationRow>
                <Switch
                  checked={field?.validation?.required || false}
                  toggle={toggleRequired}
                  label='Обязательно для заполнения'
                  mt={14}
                />
              </ST.ValidationRow>
            )}
            {validation.minMax && (
              <ST.MinMaxBlock>
                <Input
                  type={FieldType.number}
                  value={field.validation.min ? `${field.validation.min}` : ''}
                  onChange={onChangeMin}
                  label={field.type === FieldType.number ? 'Мин. значение' : 'Мин. кол-во символов'}
                />
                <Input
                  type={FieldType.number}
                  value={field.validation.max ? `${field.validation.max}` : ''}
                  onChange={onChangeMax}
                  label={field.type === FieldType.number ? 'Макс. значение' : 'Макс. кол-во символов'}
                />
              </ST.MinMaxBlock>
            )}
            {validation.regexp && (
              <ST.ValidationRow>
                <Input
                  value={field.validation.pattern || ''}
                  onChange={onChangeRegexp}
                  label='Шаблон ввода'
                />
              </ST.ValidationRow>
            )}
          </ST.Validations>
        )}
      </ST.Body>
    </Accordion>
  );
};

export default FieldAccordion;
