import styled from 'styled-components';

const Block = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  row-gap: 20px;
`;

export const DefaultSettingsBlock = styled(Block)`
  max-width: 386px;
`;

export const Validations = styled(Block)`
  max-width: 422px;
`;

export const ValidationRow = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  min-height: 69.6px;
`;

export const MinMaxBlock = styled.div`
  display: flex;
  column-gap: 20px;
  width: 100%;
`;

export const Body = styled.div`
  display: flex;
  width: 100%;
  column-gap: 108px;
`;

export const FieldOptions = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  padding-left: 20px;
  row-gap: 20px;
`;

export const FieldOption = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  row-gap: 8px;
`;
