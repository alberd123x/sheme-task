import { IOption } from 'components/Select';
import { FieldType } from 'types/entities';

export const OPTIONS: IOption[] = [
  { key: 'Текстовое поле', value: FieldType.string },
  { key: 'Числовое поле', value: FieldType.number },
  { key: 'Пароль', value: FieldType.password },
  { key: 'Чекбокс', value: FieldType.checkbox },
  { key: 'Номер телефона', value: FieldType.tel },
  { key: 'Выпадающий список', value: FieldType.select },
];

export interface IValidationValue {
  required?: boolean;
  minMax?: boolean;
  regexp?: boolean;
}

export interface IValidation {
  [key: string]: IValidationValue;
}

export const VALIDATIONS: IValidation = {
  [FieldType.string]: {
    required: true,
    minMax: true,
    regexp: true,
  },
  [FieldType.number]: {
    required: true,
    minMax: true,
  },
  [FieldType.tel]: {
    required: true,
  },
  [FieldType.select]: {
    required: true,
  },
  [FieldType.checkbox]: {
    required: true,
  },
  [FieldType.password]: {
    required: true,
    minMax: true,
    regexp: true,
  }
};
