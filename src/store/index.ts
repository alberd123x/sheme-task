import { configureStore } from '@reduxjs/toolkit';
import thunk from 'redux-thunk';
import { persistStore } from 'redux-persist';
import reducer from './reducers';

export const store = configureStore({
  reducer,
  middleware: [thunk],
});

export type IState = ReturnType<typeof store.getState>;
export type IDispatch = typeof store.dispatch;
export const persistor = persistStore(store);

export default store;
