import { createAsyncThunk } from '@reduxjs/toolkit';
import {
  fetchCreateScheme,
  fetchDeleteScheme,
  fetchScheme,
  fetchSchemes,
  fetchUpdateScheme,
  ICreateSchemeBody
} from 'api/schemes';
import { IScheme } from 'types/entities';

export const getSchemes = createAsyncThunk<IScheme[], { token: string }>(
  'schemes/get',
  async ({ token }, { rejectWithValue }) => {
    try {
      const { data } = await fetchSchemes(token);
      return data;
    } catch (e) {
      return rejectWithValue('Something went wrong!');
    }
  },
);

export const getScheme = createAsyncThunk<IScheme, { id: string | number; token: string; cb?: () => void }>(
  'shemes/getOne',
  async ({ id, token, cb }, { rejectWithValue }) => {
    try {
      const { data } = await fetchScheme(id, token);
      if (cb) {
        cb();
      }
      return data;
    } catch (e) {
      return rejectWithValue('Something went wrong!')
    }
  }
);

export const deleteScheme = createAsyncThunk<number | string, { id: string | number; token: string; cb?: () => void }>(
  'shemes/deleteScheme',
  async ({ id, token, cb }, { rejectWithValue }) => {
    try {
      await fetchDeleteScheme(id, token);
      if (cb) {
        cb();
      }
      return id;
    } catch (e) {
      return rejectWithValue('Something went wrong!')
    }
  }
);

export const createScheme = createAsyncThunk<void, { body: ICreateSchemeBody; token: string; }>(
  'shemes/createScheme',
  async ({ body, token }, { rejectWithValue }) => {
    try {
      await fetchCreateScheme(body, token);
    } catch (e) {
      return rejectWithValue('Something went wrong!')
    }
  }
);

export const updateScheme = createAsyncThunk<void, { body: IScheme; token: string; }>(
  'shemes/updateScheme',
  async ({ body, token }, { rejectWithValue }) => {
    try {
      await fetchUpdateScheme(body, token);
    } catch (e) {
      return rejectWithValue('Something went wrong!')
    }
  }
);
