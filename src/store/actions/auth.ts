import { createAsyncThunk } from '@reduxjs/toolkit';
import { loginRequest } from 'api/auth';

interface ICompleteData {
  isAuth: boolean;
  token: string;
}

export const login = createAsyncThunk<ICompleteData, { username: string, password: string }>(
  'auth/login',
  async ({ username, password }, { rejectWithValue }) => {
    try {
      const { data } = await loginRequest(username, password);
      console.log(data);
      return {
        isAuth: true,
        token: data.access_token,
      };
    } catch (e) {
      return rejectWithValue('Wrong username or password');
    }
  },
);
