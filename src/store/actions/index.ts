import { authActions } from 'store/reducers/auth';
import { schemesActions } from 'store/reducers/schemes';

const actions = { ...authActions, ...schemesActions }

export default actions;
