import { IScheme } from 'types/entities';
import { createSlice } from '@reduxjs/toolkit';
import { createScheme, deleteScheme, getScheme, getSchemes, updateScheme } from 'store/actions/schemes';

const initialState = {
  list: [] as IScheme[],
  currentScheme: null as null | IScheme,
  isError: false,
  isLoading: false,
  errorMessage: '',
};

const schemesSlice = createSlice({
  name: 'schemes',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder
      .addCase(getSchemes.fulfilled, (state, action) => {
        state.list = action.payload;
        state.isError = false;
        state.isLoading = false;
        state.errorMessage = '';
      })
      .addCase(getSchemes.rejected, (state, action) => {
        state.isError = true;
        state.isLoading = false;
        state.errorMessage = action.payload as string;
      })
      .addCase(getSchemes.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(getScheme.pending, state => {
        state.isLoading = true;
      })
      .addCase(updateScheme.pending, state => {
        state.isLoading = true;
      })
      .addCase(updateScheme.fulfilled, (state) => {
        state.isError = false;
        state.errorMessage = '';
        state.isLoading = false;
      })
      .addCase(updateScheme.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.errorMessage = action.payload as string;
      })
      .addCase(createScheme.pending, state => {
        state.isLoading = true;
      })
      .addCase(createScheme.fulfilled, (state) => {
        state.isError = false;
        state.errorMessage = '';
        state.isLoading = false;
      })
      .addCase(createScheme.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.errorMessage = action.payload as string;
      })
      .addCase(deleteScheme.pending, state => {
        state.isLoading = true;
      })
      .addCase(deleteScheme.fulfilled, (state, action) => {
        state.list = state.list.filter(item => +item.id !== +action.payload);
        state.isError = false;
        state.errorMessage = '';
        state.isLoading = false;
      })
      .addCase(deleteScheme.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.errorMessage = action.payload as string;
      })
      .addCase(getScheme.fulfilled, (state, action) => {
        state.currentScheme = action.payload;
        state.isError = false;
        state.errorMessage = '';
        state.isLoading = false;
      })
      .addCase(getScheme.rejected, (state, action) => {
        state.isLoading = false;
        state.isError = true;
        state.errorMessage = action.payload as string;
      });
  }
});

export default schemesSlice.reducer;
export const schemesActions = { ...schemesSlice.actions, getSchemes, getScheme, deleteScheme, createScheme, updateScheme };
