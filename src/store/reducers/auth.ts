import { createSlice } from '@reduxjs/toolkit';
import { login } from 'store/actions/auth';

const mockToken = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFkbWluIiwic3ViIjoyNywiaWF0IjoxNjM1MDI2MzgwfQ.Q0Kv5pRapYNzUCmjhE7My2x80GH822rlr4_AgGWZArk';

const initialState = {
  isAuth: true,
  token: mockToken as string | null,
  isError: false,
  isLoading: false,
  errorMessage: '',
};

const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    logout: state => {
      state.isAuth = false;
      state.token = null;
    },
    setAuth: state => {
      state.isAuth = true;
    },
    disableError: state => {
      state.isError = false;
    }
  },
  extraReducers: builder => {
    builder
      .addCase(login.fulfilled, (state, action) => {
        state.isAuth = action.payload.isAuth;
        state.token = action.payload.token;
        state.isLoading = false;
      })
      .addCase(login.rejected, (state, action) => {
        state.isError = true;
        state.isLoading = false;
        state.errorMessage = action.payload as string;
      })
      .addCase(login.pending, (state) => {
        state.isLoading = true;
      });
  }
});

export default authSlice.reducer;
export const authActions = { ...authSlice.actions, login };
