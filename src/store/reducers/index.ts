import auth from 'store/reducers/auth';
import schemes from 'store/reducers/schemes';
import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

export const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['isError', 'errorMessage', 'isLoading'],
};

export default combineReducers({
  auth: persistReducer(persistConfig, auth),
  schemes,
});
