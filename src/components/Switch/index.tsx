import React, { FC } from 'react';
import * as ST from './styled';

interface IProps {
  checked?: boolean;
  toggle: () => void;
  disabled?: boolean;
  label?: string;
  mt?: number;
}

const Switch: FC<IProps> = ({ disabled, toggle, label, checked, mt }) => {
  return (
    <ST.Wrapper disabled={disabled} onClick={() => disabled || toggle()} mt={mt}>
      <ST.SwitchContainer checked={checked}>
        <ST.Track>
          <ST.Control checked={checked} />
        </ST.Track>
      </ST.SwitchContainer>
      <ST.Label>{label}</ST.Label>
    </ST.Wrapper>
  );
};

export default Switch;
