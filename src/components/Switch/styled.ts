import styled from 'styled-components';
import { COLORS, OPACITY } from 'constants/utils';

export const Wrapper = styled.div<{ disabled?: boolean; mt?: number }>`
  display: flex;
  column-gap: 20px;
  opacity: ${({ disabled }) => disabled ? 0.2 : 1};
  width: fit-content;
  cursor: pointer;
  margin-top: ${({ mt }) => mt || 0}px;
`;

export const Track = styled.div`
  position: relative;
  display: flex;
  align-items: center;
  width: 100%;
  height: 100%;
`;

export const SwitchContainer = styled.div<{ checked?: boolean }>`
  min-width: 35px;
  min-height: 22px;
  background-color: ${({ checked }) => checked ? COLORS.blue : COLORS.darkGrey + OPACITY['30'] };
  border-radius: 19px;
  transition: background-color .3s ease, opacity .2s ease;
  
  :hover {
    opacity: 0.8;
  }
`;

export const Control = styled.div<{ checked?: boolean }>`
  position: absolute;
  top: 3.3px;
  left: ${({ checked }) => checked ? 15 : 3}px;
  width: 16px;
  height: 16px;
  border-radius: 50%;
  background: ${COLORS.primaryWhite};
  transition: left .3s ease;
`;

export const Label = styled.label`
  font-size: 18px;
  line-height: 22px;
  color: ${COLORS.dark};
  cursor: pointer;
  user-select: none;
`;
