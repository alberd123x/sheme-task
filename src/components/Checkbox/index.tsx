import React, { FC } from 'react';
import * as ST from './styled';

interface IProps {
  id: string;
  checked: boolean;
  onChange: () => void;
  className?: string;
  disabled?: boolean;
  label?: string;
  mr?: number;
}

const Checkbox: FC<IProps> = ({
  onChange,
  id = 'checkbox',
  checked,
  className,
  disabled,
  children,
  mr,
  label,
  ...rest
}) => {
  const change = disabled ? (()=>{}) : (onChange);
  return (
    <>
      <ST.Input id={id} type='checkbox' onChange={change} checked={checked} disabled={disabled} {...rest}/>
      <ST.Label htmlFor={id} className={className}>
        <ST.Checkmark mr={mr ? mr : children || label ? 20 : 0} disabled={disabled} />
        {label || children}
      </ST.Label>
    </>
  );
}

export default Checkbox;
