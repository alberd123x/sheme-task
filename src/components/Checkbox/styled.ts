import styled from 'styled-components';
import arrow from 'assets/images/arrow.svg';
import { COLORS, OPACITY } from 'constants/utils';

export const Label = styled.label<{ disabled?: boolean }>`
    display: flex;
    font-size: 18px;
    line-height: 22px;
    color: ${COLORS.dark};
    opacity: ${({ disabled }) => disabled ? 0.3 : 1};
    cursor: pointer;
`;

export const Input = styled.input`
    position: absolute;
    left: -9999px;
`;

export const Checkmark = styled.div<{ mr?: number, disabled?: boolean }>`
    width: 20px;
    height: 20px;
    border: 2px solid ${({ disabled }) => disabled ? COLORS.darkGrey : COLORS.blue + OPACITY['30']};
    background: ${({ disabled }) => disabled ? COLORS.grey : COLORS.primaryWhite};
    border-radius: 3px;
    position: relative;
    flex-shrink: 0;
    margin-right: ${({ mr }) => mr || 0}px;
    transition: all .1s ease; 
    :hover {
        border: 2px solid ${COLORS.blue};
    }
    ${Input}:checked + ${Label} & {
        border: none;
        background: ${props => props.disabled ? COLORS.grey + OPACITY['30'] : COLORS.blue};
        :hover {
            background: ${props => props.disabled ? COLORS.grey + OPACITY['30'] : COLORS.blue + OPACITY['30']};
            border: none;
        }
        :before {
            position: absolute;
            display: block;
            content: '';
            width: 12px;
            height: 18px;
            top: 1px;
            left: 4px;
            background:  url(${arrow}) no-repeat center center;
            transition: 0.25s;
        }  
    }
`;
