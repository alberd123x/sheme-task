import styled from 'styled-components';

export const Svg = styled.svg.attrs({
  width: '24',
  height: '24',
  viewBox: '0 0 24 24',
  fill: 'none',
  xmlns: 'http://www.w3.org/2000/svg',
})`
  cursor: pointer;
`;
