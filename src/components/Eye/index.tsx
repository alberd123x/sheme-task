import React, { FC } from 'react';
import { COLORS } from 'constants/utils';
import * as ST from './styled';

interface IProps {
  isOn?: boolean;
  isActive?: boolean;
  onClick: () => void;
}

const EYE_ON_D1 = 'M1 12C1 12 5 4 12 4C19 4 23 12 23 12C23 12 19 20 12 20C5 20 1 12 1 12Z';
const EYE_OFF_D1 = 'M14.12 14.12C13.8454 14.4148 13.5141 14.6512 13.1462 14.8151C12.7782 14.9791 12.3809 15.0673 11.9781 15.0744C11.5753 15.0815 11.1752 15.0074 10.8016 14.8565C10.4281 14.7056 10.0887 14.4811 9.80385 14.1962C9.51897 13.9113 9.29439 13.572 9.14351 13.1984C8.99262 12.8249 8.91853 12.4247 8.92563 12.0219C8.93274 11.6191 9.02091 11.2219 9.18488 10.8539C9.34884 10.4859 9.58525 10.1547 9.88 9.88003M17.94 17.94C16.2306 19.243 14.1491 19.9649 12 20C5 20 1 12 1 12C2.24389 9.68192 3.96914 7.65663 6.06 6.06003L17.94 17.94ZM9.9 4.24002C10.5883 4.0789 11.2931 3.99836 12 4.00003C19 4.00003 23 12 23 12C22.393 13.1356 21.6691 14.2048 20.84 15.19L9.9 4.24002Z';
const EYE_OFF_D2 = 'M1 1L23 23';
const EYE_ONN_D2 = 'M12 15C13.6569 15 15 13.6569 15 12C15 10.3431 13.6569 9 12 9C10.3431 9 9 10.3431 9 12C9 13.6569 10.3431 15 12 15Z';

const Eye: FC<IProps> = ({isOn, isActive, onClick}) => {
  const strokeColor = isActive ? COLORS.blue : COLORS.svgColor;
  return (
    <ST.Svg onClick={onClick}>
      <path
        d={isOn ? EYE_ON_D1 : EYE_OFF_D1}
        stroke={strokeColor}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d={isOn ? EYE_ONN_D2 : EYE_OFF_D2}
        stroke={strokeColor}
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </ST.Svg>
  );
};

export default Eye;
