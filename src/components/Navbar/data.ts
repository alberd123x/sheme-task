import { RoutesName } from 'router/routes';

interface INavButton {
  text: string;
  path: RoutesName;
}

export const NAV_BUTTONS: INavButton[] = [
  { text: 'Мои схемы', path: RoutesName.SCHEMES },
  { text: 'Создать схему', path: RoutesName.SCHEME_CREATE },
];
