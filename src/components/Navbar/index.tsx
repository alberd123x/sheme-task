import React, { FC } from 'react';
import { NAV_BUTTONS } from 'components/Navbar/data';
import { useHistory, useLocation } from 'react-router-dom';
import { useActions } from 'hooks/useActions';
import * as ST from './styled';
import { RoutesName } from 'router/routes';

const Navbar: FC = () => {
  const { push } = useHistory();
  const { pathname } = useLocation();
  const { logout } = useActions();

  const handleClick = (path: string) => {
    push(path);
  };

  return (
    <ST.Wrapper>
      <ST.StyledContainer>
        <ST.LeftSide>
          <ST.StyledLogo onClick={() => handleClick(RoutesName.SCHEMES)} />
          {NAV_BUTTONS.map(({ text, path  }) => (
            <ST.NavButton key={path} onClick={() => handleClick(path)} isActive={path === pathname}>{text}</ST.NavButton>
          ))}
        </ST.LeftSide>
        <ST.NavButton withoutBorder onClick={logout}>Выйти</ST.NavButton>
      </ST.StyledContainer>
    </ST.Wrapper>
  );
};

export default Navbar;
