import styled, { css } from 'styled-components';
import { COLORS, OPACITY } from 'constants/utils';
import { Container } from 'styled';
import Logo from 'components/Logo';

export const Wrapper = styled.div`
  position: fixed;
  top: 0;
  z-index: 9999;
  width: 100%;
  height: 80px;
  background: ${COLORS.primaryWhite};
  border-bottom: 1px solid ${COLORS.grey};
`;

export const StyledLogo = styled(Logo).attrs({
  rect: 48,
})`
  margin-right: 20px;
`;

export const NavButton = styled.div<{ isActive?: boolean; withoutBorder?: boolean }>`
  position: relative;
  height: 100%;
  padding: 0 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${COLORS.blue};
  transition: .3s ease;
  font-size: 18px;
  line-height: 28px;
  cursor: pointer;
  
  ${({ withoutBorder, isActive }) => !withoutBorder && isActive && css`
    :before {
      content: '';
      position: absolute;
      bottom: -1px;
      height: 2px;
      width: 102px;
      background: ${COLORS.blue};
    }
  `}
  
  :hover {
    background: ${COLORS.lightBlue + OPACITY['10']};
  }
`;

export const LeftSide = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
`;

export const StyledContainer = styled(Container)`
  display: flex;
  height: 100%;
  align-items: center;
  justify-content: space-between;
`;
