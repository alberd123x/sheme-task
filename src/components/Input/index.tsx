import React, { FC, useEffect, useRef, useState } from 'react';
import { AnyMaskedOptions, InputMask, Masked } from 'imask';
import { useActions } from 'hooks/useActions';
import Eye from 'components/Eye';
import { FieldType } from 'types/entities';
import * as ST from './styled';

export enum InputScheme {
  default = 'default',
  login = 'login',
}

interface IProps {
  value: string;
  type?: FieldType;
  scheme?: InputScheme;
  onChange: (v: string) => void;
  validate?: (v: string) => boolean;
  errorMessage?: string | JSX.Element;
  onError?: (v: boolean) => void;
  label?: string;
  placeholder?: string;
  isError?: boolean;
  required?: boolean;
  min?: number;
  max?: number;
  maxWidth?: string;
  icon?: string;
  onIconClick?: () => void;
}

const MASK: { [key: string]: string } = {
  [FieldType.tel]: '+{7}(000)000-00-00'
};
const VALIDATE_REGEXP: { [key: string]: RegExp } = {
  [FieldType.tel]: /^((\+7|7|8)+([0-9]){10})$/,
  [FieldType.number]: /^[+-]?\d+$/i,
};

const Input: FC<IProps> = ({
  type,
  value,
  onChange,
  label,
  errorMessage,
  isError,
  min,
  max,
  required,
  maxWidth,
  placeholder,
  scheme = InputScheme.default,
  icon,
  onIconClick,
}) => {
  const [hasError, setHasError] = useState(false);
  const [showError, setShowError] = useState(false);
  const [touched, setTouched] = useState(false);
  const [showText, setShowText] = useState(false);
  const [inFocus, setInFocus] = useState(false);
  const [tempValue, setTempValue] = useState<string | null>(null);
  const {disableError} = useActions();
  const inputRef = useRef<HTMLInputElement | null>(null);
  const maskRef = useRef<InputMask<AnyMaskedOptions> | undefined>();

  useEffect(() => {
    const mask = type && MASK[type];
    const input = inputRef.current;
    if (mask && input) {
      import('imask').then(({ default: IMask }) => {
        // @ts-ignore
        maskRef.current = IMask(input, {
          mask,
          prepare: (added: string, masked: Masked<RegExp>) => {
            if (type === FieldType.tel && added === '8' && masked.value === '') {
              return '';
            }
            return added;
          },
        });
        maskRef.current?.alignCursor();
      });
    }
    if (value || input?.value) {
      setTouched(true);
    }

    return () => {
      if (maskRef.current && maskRef.current.destroy) {
        maskRef.current?.destroy();
      }
    }
    // eslint-disable-next-line
  }, [type]);

  useEffect(() => {
    const mask = type && MASK[type];
    if (!mask && maskRef.current && maskRef.current.destroy) {
      maskRef.current?.destroy();
    }
  }, [type]);


  useEffect(() => {
    if (value) {
      setTouched(true);
    }
  }, [value]);

  const onClick = () => {
    const input = inputRef.current;
    input?.focus();
    setTouched(true);
  }

  const handleEyeClick = () => {
    setShowText(prev => !prev);
  };

  const handleFocus = () => {
    setInFocus(true);
  };

  const applyError = (value = true) => {
    setHasError(value);
  };

  const getValue = (
    event: React.ChangeEvent<HTMLInputElement> | React.KeyboardEvent<HTMLInputElement>,
    regexpToReplace?: RegExp,
    replaceTo?: string,
    convertFunc?: (value: string) => string,
  ) => {
    // @ts-ignore
    let value = maskRef.current ? maskRef.current.unmaskedValue : event.target.value;
    if (regexpToReplace) {
      value = value.replace(regexpToReplace, replaceTo ? replaceTo : '');
    }
    if  (convertFunc) {
      value = convertFunc(value);
    }
    return value;
  }

  const validate = (value: string) => {
    return new Promise(resolve => {
      const exp = type && VALIDATE_REGEXP[type];
      if (exp) {
        let invalid = !exp.test(value);
        if ((min && +value < min) || (max && +value > max)) {
          invalid = true;
        }

        if (!required && value === '') invalid = false;
        if (hasError !== invalid) {
          applyError(!hasError);
          resolve(hasError);
        } else {
          resolve(!hasError);
        }
      } else {
        resolve(true);
      }
    });
  };

  const change = (
    event: React.ChangeEvent<HTMLInputElement>,
    regexpToReplace?: RegExp,
    replaceTo?: string,
    convertFunc?: (value: string) => string,
  ) => {
    if (maskRef.current) {
      maskRef.current.updateValue();
    }
    setTempValue(event.target.value);
    let value = getValue(event, regexpToReplace, replaceTo, convertFunc);
    setShowError(true);
    check(value).then((isValid) => {
      if (isValid) {
        onChange(value);
      }
    });
  }

  const check = (value: string) => {
    return new Promise(resolve => {
      if (value) {
        setTouched(true);
        validate(value).then(res => {
          resolve(res);
        });
      } else {
        resolve(true);
      }
    });
  };

  const onPaste = (event: React.ClipboardEvent<HTMLInputElement>) => {
    if (maskRef.current) {
      let masked = maskRef.current.masked;
      masked.value = event.clipboardData.getData('text');
      onChange(masked.unmaskedValue);
    }
  };

  const onKeyPress = (
    e: React.KeyboardEvent<HTMLInputElement>,
    regexpToReplace?: RegExp,
    replaceTo?: string,
    convertFunc?: (value: string) => string,
  ) => {
    if (e.which === 13) {
      setTempValue(null);
      const value = getValue(e, regexpToReplace, replaceTo, convertFunc);
      check(value).then(isValid => {
        if (isValid) {
          onChange(value);
        }
      })
    }
  };

  const blur = (
    event: React.FocusEvent<HTMLInputElement>,
    regexpToReplace?: RegExp,
    replaceTo?: '',
    convertFunc?: (val: string) => string,
  ) => {
    const value = getValue(
      event,
      regexpToReplace,
      replaceTo,
      convertFunc
    );

    check(value)
      .then(isValid => {
        if (isValid) {
          disableError();
          setInFocus(false);
        }
        setShowError(hasError);
      });
  };

  const attrs: {
    onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
    onBlur: (e: React.FocusEvent<HTMLInputElement>) => void;
  } = {
    onChange: e => change(e),
    onBlur: e => blur(e),
  }
  let inputType: string = type || 'text';

  switch (type) {
    case FieldType.string:
      inputType = 'text';
      break;
    case FieldType.number:
      inputType = 'number';
      attrs.onChange = e => change(e, /\D/g);
      break;
    case FieldType.tel:
      inputType = 'tel';
      attrs.onChange = e => change(e, /\D/g);
      break;
    case FieldType.password:
      inputType = showText ? 'text' : 'password';
      attrs.onChange = e => change(e, /\s/ig);
      break;
    default:
      break;
  }

  return (
    <ST.Container maxWidth={maxWidth}>
      <ST.InputLabel isError={isError || showError} scheme={scheme}>
        {required && scheme === InputScheme.default && <span>*</span>}
        {label}
      </ST.InputLabel>
      <ST.InputWrapper inFocus={inFocus} isError={isError || hasError} touched={touched} onClick={onClick}>
        <ST.Input
          isError={isError}
          inFocus={inFocus}
          touched={touched}
          type={inputType}
          ref={inputRef}
          value={tempValue || value}
          onFocus={handleFocus}
          onPaste={onPaste}
          onKeyPress={onKeyPress}
          placeholder={placeholder}
          {...attrs}
        />
        {type === 'password' && <Eye isOn={showText} isActive={inFocus} onClick={handleEyeClick}/>}
        {icon && <ST.Icon src={icon} alt="icon" onClick={onIconClick} />}
      </ST.InputWrapper>
      {(isError || showError) && <ST.ErrorMessage>{errorMessage}</ST.ErrorMessage>}
    </ST.Container>
  );
};

export default Input;
