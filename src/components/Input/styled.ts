import styled, { css } from 'styled-components';
import { COLORS, FONTS, OPACITY } from 'constants/utils';
import { InputScheme } from 'components/Input/index';

export const Input = styled.input<{ touched?: boolean; isError?: boolean, inFocus?: boolean }>`
  font-family: ${FONTS.regular};
  appearance: initial !important;
  font-size: 20px;
  line-height: 24px;
  width: calc(100% - 24px);
  color: ${COLORS.dark} !important;
  outline: none;
  border: none;
  background: transparent !important;

  :-webkit-autofill,
  :-webkit-autofill:hover,
  :-webkit-autofill:focus,
  :-webkit-autofill:active {
    -webkit-box-shadow: 0 0 0 30px ${({isError, inFocus}) => {
      if (isError) {
        return COLORS.red;
      }
      
      if (inFocus) {
        return 'transparent';
      }
      return COLORS.primaryWhite;
    }} inset !important;
  }

  ::-webkit-outer-spin-button,
  ::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  ::placeholder {
    font-size: 20px;
    line-height: 24px;
    color: ${COLORS.dark};
    opacity: 0.3;
  }

  :-webkit-autofill {
    background: transparent !important;
  }
`;

export const Container = styled.div<{ maxWidth?: string }>`
  display: flex;
  flex-direction: column;
  width: 100%;
  ${({ maxWidth }) => css`max-width: ${maxWidth};`}
`;

export const InputWrapper = styled.div<{ touched: boolean; isError?: boolean; inFocus?: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  padding: 12px 15px;
  width: 100%;
  background: ${({isError, inFocus}) => {
    if (isError) {
      return `${COLORS.red}${OPACITY['10']}`;
    }
    
    if (inFocus) {
      return `${COLORS.blue}${OPACITY['10']}`;
    }
    return COLORS.primaryWhite;
  }};
  border: ${({inFocus, isError}) => {
    if (isError) {
      return `1px solid ${COLORS.red}`;
    }
    
    if (inFocus) {
      return `1px solid ${COLORS.blue}`;
    }
    return `1px solid ${COLORS.darkGrey}${OPACITY['20']}`;
  }};
  border-radius: 5px;
  max-height: 48px;
  height: 100%;
  transition: .3s ease;
  
  :hover {
    ${({ inFocus, isError }) => isError ? COLORS.red + OPACITY['10'] : !inFocus && css`background: ${COLORS.lightBlue + OPACITY['10']};`};
    border: 1px solid ${({ isError }) => isError ? COLORS.red : COLORS.blue};
  }
`;

export const ErrorMessage = styled.div`
  position: relative;
  top: 7px;
  font-size: 12px;
  line-height: 1;
  color: ${COLORS.red};
`;

export const InputLabel = styled.div<{ scheme: InputScheme; isError?: boolean }>`
  font-family: ${({ scheme }) => scheme === InputScheme.default ? FONTS.regular : FONTS.medium};
  font-size: 16px;
  color: ${({ scheme }) => scheme === InputScheme.default ? COLORS.dark : COLORS.darkGrey + OPACITY['30']};
  ${({ isError }) => isError && css`color: ${COLORS.red};`}
  line-height: 22px;
  
  span {
    color: ${COLORS.red};
    margin-right: 4px;
  }
`;

export const Icon = styled.img`
  cursor: pointer;
`;
