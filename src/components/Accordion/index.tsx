import React, { FC } from 'react';
import * as ST from './styled';

interface IProps {
  title: string;
  open: boolean;
  toggle: () => void;
  onDelete: () => void;
  key?: string;
  type?: string;
}

const Accordion: FC<IProps> = ({key, onDelete, type, toggle, open, title, children, ...rest}) => {
  return (
    <ST.Wrapper {...rest}>
      <ST.AccordionHeader>
        <ST.LeftBlock onClick={toggle}>
          <ST.Icon open={open} />
          <ST.InfoBlock>
            <ST.Title>{title}</ST.Title>
            {(key || type) && <ST.Label>{key};{type}</ST.Label>}
          </ST.InfoBlock>
        </ST.LeftBlock>
        <ST.DeleteButton onClick={onDelete} />
      </ST.AccordionHeader>
      <ST.AccordionBody open={open}>
        <ST.Border/>
        {children}
      </ST.AccordionBody>
    </ST.Wrapper>
  );
};

export default Accordion;
