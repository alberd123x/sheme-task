import styled from 'styled-components';
import { COLORS, OPACITY } from 'constants/utils';
import deleteIcon from 'assets/images/delete.svg';
import arrow from 'assets/images/arrow-accordion.svg';

export const Wrapper = styled.div`
  flex-direction: column;
  width: 100%;
`;

export const AccordionHeader = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  justify-content: space-between;
  flex-wrap: nowrap;
  cursor: pointer;
  height: 84px;
  background-color: ${COLORS.primaryWhite};
  transition: background-color .2s ease;
  
  :hover {
    background-color: ${COLORS.background};
  }
`;

export const InfoBlock = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 11px;
`;

export const Label = styled.div`
  font-size: 14px;
  line-height: 22px;
  color: ${COLORS.dark + OPACITY['60']};
`;

export const Title = styled.div`
  font-size: 18px;
  line-height: 22px;
  color: ${COLORS.dark};
`;

export const LeftBlock = styled.div`
  display: flex;
  padding: 20px;
  flex-grow: 1;
`;

export const Icon = styled.div<{ open: boolean }>`
  position: relative;
  width: 10px;
  height: 5px;
  top: ${({ open }) => open ? 6 : 6}px;
  transform: rotate(${({ open }) => open ? 0 : -90}deg);
  transition: all 0.2s ease-in;
  background: url(${arrow}) no-repeat center center;
`;

export const Border = styled.div`
  height: 1px;
  width: 100%;
  background: ${COLORS.darkGrey + OPACITY['30']};
  margin-bottom: 20px;
`;

export const AccordionBody = styled.div<{ open?: boolean }>`
  width: 100%;
  max-height: ${({ open }) => open ? '100%' : '0'};
  overflow: hidden;
  transition: max-height 0.2s ${({ open }) => open ? 'ease-in' : 'ease-out'};
`;

export const DeleteButton = styled.div`
  width: 32px;
  height: 32px;
  background: url(${deleteIcon}) no-repeat center center;
  transition: opacity .2s ease;
  
  :hover {
    opacity: 0.6;
  }
`;
