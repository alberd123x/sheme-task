import styled from 'styled-components';
import { COLORS, OPACITY } from 'constants/utils';

export const Container = styled.div`
  display: flex;
  column-gap: 20px;
`;

export const Label = styled.label<{ disabled?: boolean, checked?: boolean }>`
  display: inline-block;
  cursor: pointer;
  position: relative;
  padding-left: 25px;
  margin-right: 0;
  font-size: 18px;
  line-height: 22px;
  user-select: none;
  opacity: ${({ disabled }) => disabled ? 0.3 : 1};
  color: ${({ disabled }) => disabled ? COLORS.grey : COLORS.dark};

  :before {
    content: "";
    display: inline-block;
    width: 20px;
    height: 20px;
    position: absolute;
    left: 0;
    bottom: 1px;
    border: ${({ checked, disabled }) => 
            `${checked ? 5 : 2}px solid ${disabled ? COLORS.darkGrey + OPACITY['30'] : checked ? COLORS.blue + OPACITY['30'] : COLORS.blue}`};
    border-radius: 50%;
  }
`;

export const Radio = styled.input.attrs({
  type: 'radio',
})`
  display: none;
`;
