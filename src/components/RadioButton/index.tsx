import React, { FC } from 'react';
import * as ST from './styled';

interface IProps {
  id: string;
  checked?: boolean;
  disabled?: boolean;
  label?: string
  onChange: () => void;
}

const RadioButton: FC<IProps> = ({ id, checked, disabled, onChange, label }) => {
  return (
    <ST.Container>
      <ST.Radio id={id} checked={checked} disabled={disabled} />
      <ST.Label htmlFor={id} checked={checked} disabled={disabled}>{label}</ST.Label>
    </ST.Container>
  );
};

export default RadioButton;
