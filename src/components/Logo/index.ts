import styled from 'styled-components';
import logo from 'assets/images/sibdev-logo.svg';

const Logo = styled.div<{ rect?: number }>`
  width: ${({ rect }) => rect || 88}px;
  height: ${({ rect }) => rect || 88}px;
  background: url(${logo}) no-repeat center center;
  background-size: contain;
`;

export default Logo;
