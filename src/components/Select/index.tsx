import React, { FC, useRef, useState } from 'react';
import { useOutsideHandler } from 'hooks/useOutsideHandler';
import ModalPortal from 'components/ModalPortal';
import * as ST from './styled';

export interface IOption {
  value: string;
  key: string;
}

interface IProps {
  options: IOption[];
  placeholder: string;
  onChange: (option: string) => void;
  value?: string;
  label?: string;
  required?: boolean;
}

const Select: FC<IProps> = ({ placeholder, value, onChange, options, label, required }) => {
  const [open, setOpen] = useState(false);
  const selectRef = useRef<HTMLDivElement>(null);
  const optionsRef = useRef<HTMLDivElement>(null);
  const dropdownStylesRef = useRef({
    left: 0,
    top: 0,
    width: 0,
  });

  const closeSelect = () => {
    setOpen(false);
  };

  const toggleModal = () => {
    setOpen(prevState => !prevState);
  };

  const openDropdownMenu = (e: React.MouseEvent<HTMLDivElement>) => {
    const rect = selectRef.current?.getBoundingClientRect();
    if (rect) {
      dropdownStylesRef.current = {
        left: rect.x,
        top: rect.y + window.scrollY + rect.height,
        width: rect.width,
      };
    }
    toggleModal();
  }

  useOutsideHandler(optionsRef, closeSelect, selectRef);

  return (
    <ST.Container>
      {label && (
        <ST.Label>
          {required && <span>*</span>} {label}
        </ST.Label>
      )}
      <ST.SelectWrapper open={open} ref={selectRef} onClick={openDropdownMenu}>
        {placeholder && !value && <ST.Placeholder>{placeholder}</ST.Placeholder>}
        {value && <ST.Value>{value}</ST.Value>}
        <ST.Indicator open={open} />
        {open && (
          <ModalPortal>
            <ST.Options ref={optionsRef} style={dropdownStylesRef.current}>
              {options.map(option => (
                <ST.Option key={option.value} onClick={() => onChange(option.value)} isActive={option.key === value}>{option.key}</ST.Option>
              ))}
            </ST.Options>
          </ModalPortal>
        )}
      </ST.SelectWrapper>
    </ST.Container>
  );
};

export default Select;
