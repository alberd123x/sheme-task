import styled, { css } from 'styled-components';
import { COLORS, OPACITY } from 'constants/utils';
import arrow from 'assets/images/arrow-accordion.svg';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  cursor: pointer;
`;

export const Label = styled.div`
  font-size: 16px;
  line-height: 22px;
  color: ${COLORS.dark};
  
  span {
    color: ${COLORS.red};
  }
`;

export const SelectWrapper = styled.div<{ inFocus?: boolean; open?: boolean }>`
  position: relative;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 12px 15px;
  width: 100%;
  background: ${({inFocus}) => inFocus ? COLORS.blue + OPACITY['10'] : COLORS.primaryWhite};
  border: 1px solid ${({open}) => open ? COLORS.blue : COLORS.darkGrey + OPACITY['20']};
  border-radius: ${({open}) => open ? '5px 5px 0 0' : '5px'};
  ${({open}) => open && css`border-bottom: 1px solid ${COLORS.darkGrey + OPACITY['20']};`};
  max-height: 48px;
  height: 100%;
  transition: .3s ease;
  min-width: 350px;

  :hover {
    ${({inFocus}) => !inFocus && css`background: ${COLORS.lightBlue + OPACITY['10']};`};
    border: 1px solid ${COLORS.blue};
  }
`;

export const Placeholder = styled.div`
  font-size: 20px;
  line-height: 24px;
  color: ${COLORS.dark};
  opacity: 0.3;
`;

export const Value = styled.div`
  font-size: 20px;
  line-height: 24px;
  width: calc(100% - 24px);
  color: ${COLORS.dark};
`;

export const Indicator = styled.div<{ open?: boolean }>`
  position: relative;
  width: 10px;
  height: 5px;
  top: 0;
  transform: rotate(${({open}) => open ? 0 : -90}deg);
  transition: all 0.2s ease-in;
  background: url(${arrow}) no-repeat center center;
`;

export const Options = styled.div`
  position: absolute;
  width: 100%;
  border: 1px solid ${COLORS.blue};
  border-top: none;
  border-radius: 0 0 5px 5px;
`;

export const Option = styled.div<{ isActive?: boolean }>`
  display: flex;
  align-items: center;
  cursor: pointer;
  width: 100%;
  padding: 12px 14px;
  font-size: 20px;
  line-height: 48px;
  max-height: 48px;
  color: ${COLORS.dark};
  background-color: ${({isActive}) => isActive ? '#ecf5fb' : COLORS.primaryWhite};
  transition: background-color .2s ease;

  :hover {
    background-color: #ecf5fb;
  }
`;
