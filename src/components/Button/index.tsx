import React, { FC } from 'react';
import * as ST from './styled';
import { TCustomStyles } from 'types/utils';

export enum ButtonSchemes {
  primary = 'primary',
  inline = 'inline',
  error = 'error',
}

interface IProps {
  scheme?: ButtonSchemes;
  text?: string;
  disabled?: boolean;
  onClick: () => void;
  customStyles?: TCustomStyles;
}

const Button: FC<IProps> = ({
                              scheme = ButtonSchemes.primary,
                              disabled,
                              text = 'Продолжить',
                              children,
                              onClick,
                              customStyles,
                              ...props
}) => {
  return (
    <ST.Container scheme={scheme} disabled={disabled} onClick={() => disabled || onClick()} customStyles={customStyles} {...props}>
      {text || children}
    </ST.Container>
  );
};

export default Button;
