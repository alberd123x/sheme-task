import styled from 'styled-components';
import { COLORS } from 'constants/utils';
import { TCustomStyles } from 'types/utils';
import { ButtonSchemes } from 'components/Button/index';

export const Container = styled.button<{ customStyles?: TCustomStyles; scheme: ButtonSchemes }>`
  display: flex;
  justify-content: center;
  align-items: center;
  column-gap: 4px;
  width: 100%;
  padding: 14px 20px;
  background: ${({scheme}) => {
    switch (scheme) {
      case ButtonSchemes.primary:
        return COLORS.blue;
      case ButtonSchemes.inline:
      case ButtonSchemes.error:
        return 'transparent';
      default:
        return COLORS.blue;
    }
  }};
  color: ${({scheme}) => {
    switch (scheme) {
      case ButtonSchemes.primary:
        return COLORS.primaryWhite;
      case ButtonSchemes.inline:
        return COLORS.blue;
      case ButtonSchemes.error:
        return COLORS.red;
      default:
        return COLORS.primaryWhite;
    }
  }};
  border-radius: 4px;
  height: 52px;
  outline: none;
  border: ${({scheme}) => {
    switch (scheme) {
      case ButtonSchemes.primary:
        return 'none';
      case ButtonSchemes.inline:
        return `1px solid ${COLORS.blue}`;
      case ButtonSchemes.error:
        return `1px solid ${COLORS.red}`;
      default:
        return 'none';
    }
  }};
  font-size: 20px;
  line-height: 24px;
  transition: background .3s ease, color .3s ease;
  cursor: pointer;

  :disabled {
    opacity: 0.5;
  }

  :hover {
    opacity: 0.6;

    :disabled {
      opacity: 0.5;
    }
  }

  ${({customStyles}) => customStyles}
`;

export const ErrorIcon = styled.img`
  width: 16px;
  height: 16px;
`;
