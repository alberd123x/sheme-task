export const getCookie = (cookieToFind: string): string | null => {
  const findedCookie = document.cookie.split('; ').find(cookie => cookie.startsWith(cookieToFind));

  if (findedCookie) {
    return findedCookie.replace(`${cookieToFind}=`, '');
  }

  return null;
};

interface IAcc {
  [key: string]: string;
}

export const getCookies = (cookiesList: string[]) => {
  const cookiesObject = {};

  const cookies = cookiesList.reduce((acc: IAcc, cookie) => {
    const param = getCookie(cookie);

    if (param) {
      acc[cookie] = param;
    }

    return acc;
  }, cookiesObject);

  return cookies;
};

export const setCookie = ({ name, value }: { name: string; value: string }) => {
  document.cookie = `${name}=${value}`;
};

export const deleteCookie = (name: string) => {
  setCookie({
    name,
    value: '',
  });
};
